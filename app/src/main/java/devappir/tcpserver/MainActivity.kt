package devappir.tcpserver

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket

class MainActivity : AppCompatActivity() {
    //Send
    private var socket: Socket? = null
    //Receive
    private var serverSocket: ServerSocket? = null
    var updateConversationHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Receive
        Thread(ServerThread()).start()


        //Send
        Thread(ClientThread()).start()
        myButton.setOnClickListener({
            Thread(ClientSend()).start()
        })
        myButtonReconnect.setOnClickListener({
            Thread(ClientThread()).start()
        })
    }

    //Send
    internal inner class ClientThread : Runnable {
        override fun run() {
            Log.i("SOCKET_TEST 0 ", "${socket != null}")
            socket = Socket(InetAddress.getByName("localhost"), 6000)
            Log.i("SOCKET_TEST 00 ", "${socket != null}")
        }
    }

    internal inner class ClientSend : Runnable {
        override fun run() {
            Log.i("SOCKET_TEST 1 ", "${socket != null}")
            if (socket != null)
                PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getOutputStream())), true).println(EditText01.text.toString())
        }
    }

    //Receive
    override fun onStop() {
        super.onStop()
        serverSocket!!.close()
    }

    internal inner class ServerThread : Runnable {
        override fun run() {
            serverSocket = ServerSocket(6000)
            while (!Thread.currentThread().isInterrupted)
                Thread(CommunicationThread(serverSocket!!.accept())).start()
        }
    }

    internal inner class CommunicationThread(private val clientSocket: Socket) : Runnable {
        private var input = BufferedReader(InputStreamReader(this.clientSocket.getInputStream()))

        override fun run() {
            while (!Thread.currentThread().isInterrupted)
                updateConversationHandler.post(UpdateUIThread(input.readLine()))
        }
    }

    internal inner class UpdateUIThread(private val msg: String) : Runnable {
        @SuppressLint("SetTextI18n")
        override fun run() {
            txvResult.text = "${txvResult.text}Client Says: $msg\n"
        }
    }
}
